<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// PDF
Route::get('/uploadFormPDF', function () {
    return view('forms.uploadPDF');
})->name('uploadFormPDF');

Route::put('/uploadPDF', 'PDFController@uploadPDF')->name('uploadPDF');

// Slider
Route::get('/slider/all', 'SliderController@getAllSliders')->name('allSlider');
Route::get('/slider', 'SliderController@index')->name('slider');

Route::get('/download-slider', 'SliderController@downloadSlider')->name('download-slider');
