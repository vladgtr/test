<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

    
// Slider
Route::group(['prefix' => 'slider'], function () {
    
    // Get images by ID
    Route::get('images/{id}', function ($id) {
        $slider = \App\Http\Controllers\SliderController::getSlider(['id' => $id]);
        
        return response()->json($slider['images']);
    });
});
