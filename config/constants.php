<?php
return [
    'pdf' => [
        'max_pdf_pages' => 21,
        'max_upload_pdf_filesize' => 50000, // In KB
    ],
    'slider' => [
        'max_time_token_url' => 1800, // In second
    ],
];
