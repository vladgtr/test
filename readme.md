## Start project 

In terminal run

    echo `id -u`:`id -g` 
    
and paste in `docker-compose.xml` in service

    php: 
        user: $(UID):$(GID)
        
and run 

    docker-compose up -d --build

## REST API Swagger Documentation

https://app.swaggerhub.com/apis-docs/test5373/Test/1.0.0-oas3

