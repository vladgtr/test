<?php
/**
 * Created by PhpStorm.
 * User: vladgtr
 * Date: 06.10.18
 * Time: 19:13
 */

namespace App\Validators;

class Pdf
{
    public function validateCountPages($attribute, $value, $parameters)
    {
        $pdf_file = $value->getPathname();
    
        $pdf_raw = file($pdf_file);
    
        $pdf_pages = '';
    
        foreach ($pdf_raw as $pdf_raw_string) {
            if (preg_match('/Count\ (\d+)/', $pdf_raw_string, $pdf_raw)) {
                $pdf_pages = $pdf_raw[1];
                break;
            }
        }
    
        if ($pdf_pages > $parameters[0]) {
            return false;
        } else {
            return true;
        }
    }
    
    public function replacerCountPages($message, $attribute, $rule, $parameters)
    {
        return 'Pdf pages > ' . $parameters[0];
    }
}
