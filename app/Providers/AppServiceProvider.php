<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Extend Validator
        
        /**
         * Validator for check the number of pages
         */
        Validator::extend('pdf_count_pages', 'App\Validators\Pdf@validateCountPages');
        Validator::replacer('pdf_count_pages', 'App\Validators\Pdf@replacerCountPages');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
