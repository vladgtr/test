<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;

class HeadComposer {
	public function compose(View $view)
	{
		$view->with('key', 'value');
	}
}