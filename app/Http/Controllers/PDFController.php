<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Shox\RandomToken;
use Wb\PdfImages\PdfImages;

class PDFController extends Controller
{
    /**
     * Parse images from upload PDF
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadPDF(Request $request)
    {
        $this->validate($request, [
            'pdf' => 'required|
			        pdf_count_pages:' . Config::get('constants.pdf.max_pdf_pages') . '|
			        mimes:pdf|
			        max:' . Config::get('constants.pdf.max_upload_pdf_filesize'),
        ]);
        
        $pdfimages = PdfImages::create();
        $pdfimages = $pdfimages->extractImages($request->file('pdf')->getPathname(), storage_path('app/public/pdf'));
        
        $token = RandomToken::generate(32);
        
        $slider = [
            'session_id' => $request->session()->getId(),
            'token'      => $token,
            'path'       => basename($pdfimages->current()->getPath()),
        ];
        
        SliderController::addSlider($slider);
        
        $responsive['url'] = route('slider', ['token' => $token]);
        
        return response()->json($responsive);
    }
}
