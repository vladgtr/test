<?php

namespace App\Http\Controllers;

use \App\Slider;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SliderController extends Controller
{
    public function index(Request $request)
    {
        $token = $request->query('token');
        
        if (is_null($token)) {
            throw new BadRequestHttpException();
        }
        
        $slider = $this->getSlider(['token' => $token]);
        
        return view('slider', $slider);
    }
    
    public static function addSlider(array $args)
    {
        start:
        
        $slider = Slider::where('token', $args['token'])->first();
        
        if (is_null($slider) || $slider->token != $args['token']) {
            $slider = Slider::create($args);
        } else {
            goto start;
        }
        
        return $slider;
    }
    
    public function getAllSliders()
    {
        $data = Slider::all();
        $result['items'] = $data->toArray();
        
        return view('allslider', $result);
    }
    
    public static function getSlider(array $args)
    {
        $where = '';
        
        if (isset($args['id'])) {
            $where = 'id = :id' . $args['id'];
        } elseif (isset($args['token'])) {
            $where = 'token = :token' . $args['token'];
        } else {
            throw new BadRequestHttpException();
        }
        
        $slider = Slider::whereRaw($where, $args)->first()->getAttributes();
        
        $view = false;
        
        if (isset($args['token'])) {
            $view = strtotime($slider['created_at']) > (time() - Config::get('constants.slider.max_time_token_url'));
        } elseif (isset($args['id'])) {
            $view = true;
        }
        
        if ($view) {
            $slider['images'] = Storage::files('/public/pdf/' . $slider['path']);
        } else {
            throw new NotFoundHttpException();
        }
        
        return $slider;
    }
    
    public function downloadSlider(Request $request)
    {
        $token = $request->query('token');
        
        if (is_null($token)) {
            throw new BadRequestHttpException();
        }
        
        $slider = $this->getSlider(['token' => $token]);
        
        $zip      = new \ZipArchive();
        $zip_path = storage_path('app/public/slider.zip');
        
        if ($zip->open($zip_path, file_exists($zip_path) ? \ZipArchive::OVERWRITE : \ZipArchive::CREATE) === true) {
            $zip->addEmptyDir('assets');
            $zip->addEmptyDir('images');
            $html = view('zip.zip', $slider)->render();
            $zip->addFromString('index.html', $html);
            $zip->addFile(public_path('/js/app.js'), '/assets/app.js');
            $zip->addPattern('/.*[^map]$/', public_path('/css'), ['add_path' => '/assets/', 'remove_all_path' => true]);
            $zip->addPattern(
                '/.*/',
                public_path('/css/fonts'),
                ['add_path' => '/assets/fonts/', 'remove_all_path' => true]
            );
            
            foreach ($slider['images'] as $image) {
                $path = storage_path('app/' . $image);
                
                if (file_exists($path)) {
                    $zip->addFile($path, '/images/' . basename($path));
                } else {
                    throw new FileNotFoundException();
                }
            }
            
            $zip->close();
        }
        
        return response()->download($zip_path);
    }
}
