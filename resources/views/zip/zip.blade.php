<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Slider</title>
    <link rel="stylesheet" href="assets/app.css">
    <style>
        .slick-track {
            height: 100vh;
        }
        img {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="slider slick-slider">
            @foreach($images as $image)
                <div class="slick-slide"><img src="{{ str_replace('public/pdf/' . $path, 'images', $image) }}" alt=""></div>
            @endforeach
        </div>
    </div>
    <script src="assets/app.js"></script>
    <script>
	    $(function () {
		    $('.slider').slick();
	    });
    </script>
</body>
</html>