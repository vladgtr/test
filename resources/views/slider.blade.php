@extends('main')

@section('styles')
<style>
    .slick-track {
        height: 100vh;
    }
    img {
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="slider slick-slider">
            @foreach($images as $image)
                <div class="slick-slide"><img src="{{ asset(str_replace('public', 'storage', $image)) }}" alt="{{ basename($image) }}"></div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <a href="{{ route('download-slider', ['token' => $token]) }}" class="btn btn-primary" download>Downloads</a>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
        $('.slider').slick();
    });
</script>
@endsection