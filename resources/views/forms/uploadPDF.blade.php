@extends('main')
<?php //phpinfo(); ?>

@section('content')
    <div class="container">
        <form action="{{ route('uploadPDF') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <label for="upload">Upload PDF file</label>
                <input type="file" name="pdf" accept="application/pdf,application/vnd.ms-excel">
            </div>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div >
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
	    $(function() {

		    var bar = $('.progress-bar');
		    var status = $('#status');

		    $('form').ajaxForm({
			    beforeSend: function() {
				    status.empty();
				    var percentVal = '0%';
				    bar.width(percentVal).html(percentVal);
			    },
			    uploadProgress: function(event, position, total, percentComplete) {
				    var percentVal = percentComplete + '%';
				    bar.width(percentVal).html(percentVal);
			    },
			    success: function(data, statusText) {
				    var percentVal = '100%';
				    if (data.url !== undefined) {
						bar.width(percentVal).html('Wait, Saving');
						alert('Uploaded Successfully');
						bar.html('Complete')
						window.location.href = data.url;
					} else {
				    	alert('Error, not found redirect link');
				  	}
			    }, 
			  	error: function (data) {
				  alert(data.responseJSON.pdf[0]);
				}
		    });

	    });
    </script>
@endsection