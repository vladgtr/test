@include('common.head')

@include('common.header')

@yield('content')

@include('common.footer')
