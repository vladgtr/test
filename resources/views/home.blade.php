@extends('main')

@section('content')
    <div class="container">
        <ul class="nav nav-pills">
            <li><a href="{{ route('uploadFormPDF') }}">Form upload PDF</a></li>
            <li><a href="{{ route('allSlider') }}">All slider IDs</a></li>
        </ul>
    </div>
@endsection