@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="text">
            REST API for get array images in JSON
            /api/slider/images/{$id}
        </div>
    </div>
    <div class="row">
        <ul>
            @foreach($items as $item)
                <li><a href="{{ URL('/api/slider/images/' . $item['id']) }}">Slider ID: {{ $item['id'] }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection